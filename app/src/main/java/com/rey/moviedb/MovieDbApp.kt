package com.rey.moviedb

import android.app.Application
import com.rey.moviedb.di.AppInjector
import com.rey.moviedb.di.DaggerAppGraph
import com.rey.moviedb.util.OpenForTesting
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

@OpenForTesting
class MovieDbApp : Application(), HasAndroidInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    fun inject() {
        DaggerAppGraph.builder().application(this).build().inject(this)
    }

    override fun onCreate() {
        super.onCreate()
        inject()

        AppInjector.init(this)
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector
}
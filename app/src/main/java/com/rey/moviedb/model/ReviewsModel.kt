package com.rey.moviedb.model

data class ReviewsModel(
    val id: Long,
    val page: Long,
    val results: List<ReviewModel>
)
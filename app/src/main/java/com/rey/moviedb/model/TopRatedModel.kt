package com.rey.moviedb.model

data class TopRatedModel(
    val page: Long,
    val results: List<MovieModel>
)
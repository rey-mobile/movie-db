package com.rey.moviedb.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ReviewModel(
    @PrimaryKey(autoGenerate = true)
    val localId: Long,
    val author: String,
    val content: String,
    val id: String,
    val url: String
) {
    var pageIndex: Long = 0
    var movieId: Long = 0
}
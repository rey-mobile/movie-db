package com.rey.moviedb.model

data class NowPlayingModel(
    val page: Long,
    val results: List<MovieModel>
)
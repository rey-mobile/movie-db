package com.rey.moviedb.model

import com.google.gson.annotations.SerializedName

data class PopularModel(
    val page: Long,
    @SerializedName("total_page") val totalPage: Long,
    @SerializedName("total_results") val totalResult: Long,
    val results: List<MovieModel>
)
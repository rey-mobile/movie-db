package com.rey.moviedb.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.rey.moviedb.repository.IRepository

@Entity
data class MovieModel(
    @PrimaryKey(autoGenerate = true)
    val localId: Long,
    val id: Long,
    @SerializedName("poster_path") var posterPath: String?,
    val overview: String,
    val title: String,
    @SerializedName("release_date") val releaseDate: String
) {
    var pageIndex: Long = 0
    var category: IRepository.Type = IRepository.Type.POPULAR
}
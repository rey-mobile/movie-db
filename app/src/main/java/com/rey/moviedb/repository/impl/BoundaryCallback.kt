package com.rey.moviedb.repository.impl

import androidx.paging.PagedList
import com.rey.moviedb.data.IData
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.Result
import com.rey.moviedb.util.wrapEspressoIdlingResource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BoundaryCallback(
    private val type: IRepository.Type,
    private val localData: IData,
    private val remoteData: IData,
    private val posterPath: String,
    private val apiKey: String,
    private val coroutineDispatcher: CoroutineDispatcher = Dispatchers.Default
) : PagedList.BoundaryCallback<MovieModel>() {

    fun requestAndSave() {
        CoroutineScope(coroutineDispatcher).launch {
            wrapEspressoIdlingResource {
                val pageIndex = localData.getLastPageIndex() + 1

                try {
                    val list: List<MovieModel> = (when (type) {
                        IRepository.Type.POPULAR -> remoteData.getPopularMovies(pageIndex, apiKey)
                        IRepository.Type.TOP_RATED -> remoteData.getTopRatedMovies(
                            pageIndex,
                            apiKey
                        )
                        IRepository.Type.NOW_PLAYING -> remoteData.getNowPlayingMovies(
                            pageIndex,
                            apiKey
                        )
                        else -> throw IllegalArgumentException("invalid type")
                    } as Result.Success).data.map {
                        it.posterPath = posterPath + it.posterPath
                        it.pageIndex = pageIndex
                        it.category = type
                        it
                    }

                    localData.insertMoviesCache(list)
                } catch (e: Exception) {

                }
            }
        }
    }

    override fun onZeroItemsLoaded() {
        requestAndSave()
    }

    override fun onItemAtEndLoaded(itemAtEnd: MovieModel) {
        requestAndSave()
    }
}
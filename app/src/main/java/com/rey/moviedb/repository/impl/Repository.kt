package com.rey.moviedb.repository.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.rey.moviedb.data.IData
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.Result
import com.rey.moviedb.util.wrapEspressoIdlingResource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(
    @Named("localData") private val localData: IData,
    @Named("remoteData") private val remoteData: IData,
    private val dispatcher: CoroutineDispatcher,
    @Named("apiKey") private val apiKey: String,
    @Named("basePosterUrl") private val basePosterUrl: String
) : IRepository {

    private lateinit var boundaryCallback: BoundaryCallback

    override fun observeMovies(type: IRepository.Type): LiveData<PagedList<MovieModel>> {
        boundaryCallback =
            BoundaryCallback(type, localData, remoteData, basePosterUrl, apiKey, dispatcher)

        return localData.observeMoviesCache(type)
            .toLiveData(pageSize = 20, boundaryCallback = boundaryCallback)
    }

    override suspend fun refreshMovies(type: IRepository.Type) {
        withContext(dispatcher) {
            val pageIndex = 1L

            try {
                val list: List<MovieModel> = (when (type) {
                    IRepository.Type.POPULAR -> remoteData.getPopularMovies(pageIndex, apiKey)
                    IRepository.Type.TOP_RATED -> remoteData.getTopRatedMovies(pageIndex, apiKey)
                    IRepository.Type.NOW_PLAYING -> remoteData.getNowPlayingMovies(
                        pageIndex,
                        apiKey
                    )
                    else -> throw IllegalArgumentException("invalid type")
                } as Result.Success).data.map {
                    it.posterPath = basePosterUrl + it.posterPath
                    it.category = type
                    it
                }

                localData.deleteAllMoviesCache(type)
                localData.insertMoviesCache(list)
            } catch (e: Exception) {

            }
        }
    }

    override fun observeFavoriteMovies(): LiveData<PagedList<MovieModel>> =
        localData.observeFavoriteMovies().toLiveData(pageSize = 20)

    override fun observeMovie(type: IRepository.Type, id: Long): LiveData<MovieModel> {
        val remoteMovieDetail = MutableLiveData<MovieModel>()

        val movieDetail = MediatorLiveData<MovieModel>()
        movieDetail.addSource(localData.observeMovieDetail(type, id)) { movieDetail.value = it }
        movieDetail.addSource(remoteMovieDetail) { movieDetail.value = it }

        CoroutineScope(dispatcher).launch {
            wrapEspressoIdlingResource {
                val movie: MovieModel
                try {
                    movie = (remoteData.getMovieDetail(id, apiKey) as Result.Success).data
                    movie.let {
                        it.posterPath = basePosterUrl + it.posterPath
                        it.category = type
                        it
                    }

                    remoteMovieDetail.postValue(movie)
                } catch (e: Exception) {

                }

                val reviews: List<ReviewModel>
                try {
                    reviews = (remoteData.getMovieReviews(id, 1, apiKey) as Result.Success).data

                    localData.insertMovieReviews(reviews)
                } catch (e: Exception) {

                }
            }
        }

        return movieDetail
    }

    override suspend fun insertFavoriteMovie(movie: MovieModel) =
        withContext(dispatcher) { localData.insertFavoriteMovie(movie) }

    override suspend fun deleteFavoriteMovie(id: Long) =
        withContext(dispatcher) { localData.deleteFavoriteMovie(id) }

    override fun observeFavoriteMovie(id: Long): LiveData<MovieModel> =
        localData.observeFavoriteMovie(id)

    override fun observeMovieReviews(id: Long): LiveData<PagedList<ReviewModel>> =
        localData.observeMovieReviews(id).toLiveData(pageSize = 20)
}
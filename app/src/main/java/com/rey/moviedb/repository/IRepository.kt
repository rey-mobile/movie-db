package com.rey.moviedb.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel

interface IRepository {
    enum class Type {
        POPULAR,
        TOP_RATED,
        NOW_PLAYING,
        FAVORITE
    }

    fun observeMovies(type: Type): LiveData<PagedList<MovieModel>>
    fun observeFavoriteMovies(): LiveData<PagedList<MovieModel>>
    fun observeMovie(type: Type, id: Long): LiveData<MovieModel>
    suspend fun refreshMovies(type: Type)
    suspend fun insertFavoriteMovie(movie: MovieModel)
    suspend fun deleteFavoriteMovie(id: Long)
    fun observeFavoriteMovie(id: Long): LiveData<MovieModel>
    fun observeMovieReviews(id: Long): LiveData<PagedList<ReviewModel>>
}
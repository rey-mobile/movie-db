package com.rey.moviedb.di

import android.app.Application
import com.rey.moviedb.MovieDbApp
import com.rey.moviedb.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class, RepositoryModule::class, DataModule::class, DatabaseModule::class,
        ApiModule::class, CoroutineModule::class, ActivityBuildersModule::class, FragmentBuildersModule::class]
)
interface AppGraph : AndroidInjector<MovieDbApp> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppGraph
    }

    override fun inject(movieDbApp: MovieDbApp)
}
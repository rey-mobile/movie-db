package com.rey.moviedb.di.module

import com.rey.moviedb.ui.fragment.FavoriteMoviesFragment
import com.rey.moviedb.ui.fragment.MovieDetailFragment
import com.rey.moviedb.ui.fragment.MoviesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeMovieFragment(): MoviesFragment

    @ContributesAndroidInjector
    abstract fun contributeMovieDetailFragment(): MovieDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoriteMovieFragment(): FavoriteMoviesFragment
}
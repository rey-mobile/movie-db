package com.rey.moviedb.di.module

import android.content.Context
import androidx.room.Room
import com.rey.moviedb.data.cache.MovieDbCache
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ApplicationContextModule::class, DaoModule::class])
class DatabaseModule {
    @Singleton
    @Provides
    fun provideMovieDbDatabase(context: Context): MovieDbCache {
        return Room.databaseBuilder(context, MovieDbCache::class.java, "moviedb")
            .fallbackToDestructiveMigration()
            .build()
    }
}
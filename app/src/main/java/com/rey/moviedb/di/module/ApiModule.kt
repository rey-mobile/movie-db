package com.rey.moviedb.di.module

import com.rey.moviedb.data.api.MovieDbApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {
    @Singleton
    @Provides
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return "https://api.themoviedb.org/3/movie/"
    }

    @Singleton
    @Provides
    @Named("apiKey")
    fun provideApiKey(): String {
        return "54ba232fbacfc92aff5e7ad7de62a823"
    }

    @Singleton
    @Provides
    @Named("basePosterUrl")
    fun provideBasePosterUrl(): String {
        return "https://image.tmdb.org/t/p/w92"
    }

    @Singleton
    @Provides
    fun provideMovieDbApi(@Named("baseUrl") baseUrl: String): MovieDbApi {
        val logger = HttpLoggingInterceptor()
        logger.level = HttpLoggingInterceptor.Level.BASIC

        val client = OkHttpClient.Builder().addInterceptor(logger).build()

        return Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create()).client(client).build()
            .create(MovieDbApi::class.java)
    }
}
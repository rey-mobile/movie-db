package com.rey.moviedb.di.module

import com.rey.moviedb.data.cache.MovieDbCache
import com.rey.moviedb.data.cache.dao.MovieDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DaoModule {
    @Singleton
    @Provides
    fun provideMovieDao(movieDbCache: MovieDbCache): MovieDao {
        return movieDbCache.getMovieDao()
    }
}
package com.rey.moviedb.di.module

import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.repository.impl.Repository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {
    @Singleton
    @Binds
    abstract fun bindRepository(repository: Repository): IRepository
}
package com.rey.moviedb.di.module

import com.rey.moviedb.data.IData
import com.rey.moviedb.data.impl.LocalData
import com.rey.moviedb.data.impl.RemoteData
import dagger.Binds
import dagger.Module
import javax.inject.Named
import javax.inject.Singleton

@Module
abstract class DataModule {
    @Singleton
    @Binds
    @Named("localData")
    abstract fun bindLocalData(localData: LocalData): IData

    @Singleton
    @Binds
    @Named("remoteData")
    abstract fun bindRemoteData(remoteData: RemoteData): IData
}
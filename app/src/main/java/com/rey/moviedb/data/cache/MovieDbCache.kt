package com.rey.moviedb.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rey.moviedb.data.cache.dao.MovieDao
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel

@Database(entities = [MovieModel::class, ReviewModel::class], version = 2)
@TypeConverters(Converters::class)
abstract class MovieDbCache : RoomDatabase() {
    abstract fun getMovieDao(): MovieDao
}
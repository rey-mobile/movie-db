package com.rey.moviedb.data

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.Result

interface IData {
    suspend fun getPopularMovies(page: Long, apiKey: String): Result<List<MovieModel>>
    suspend fun getTopRatedMovies(page: Long, apiKey: String): Result<List<MovieModel>>
    suspend fun getNowPlayingMovies(page: Long, apiKey: String): Result<List<MovieModel>>
    suspend fun getMovieDetail(id: Long, apiKey: String): Result<MovieModel>
    suspend fun getMovieReviews(id: Long, page: Long, apiKey: String): Result<List<ReviewModel>>
    suspend fun getLastPageIndex(): Long
    suspend fun insertMoviesCache(list: List<MovieModel>): List<Long>
    suspend fun insertMovieReviews(list: List<ReviewModel>): List<Long>
    suspend fun deleteAllMoviesCache(type: IRepository.Type)
    fun observeMoviesCache(type: IRepository.Type): DataSource.Factory<Int, MovieModel>
    fun observeMovieDetail(type: IRepository.Type, id: Long): LiveData<MovieModel>
    fun observeMovieReviews(id: Long): DataSource.Factory<Int, ReviewModel>
    suspend fun insertFavoriteMovie(movie: MovieModel)
    suspend fun deleteFavoriteMovie(id: Long)
    fun observeFavoriteMovies(): DataSource.Factory<Int, MovieModel>
    fun observeFavoriteMovie(id: Long): LiveData<MovieModel>
}
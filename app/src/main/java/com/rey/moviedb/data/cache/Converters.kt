package com.rey.moviedb.data.cache

import androidx.room.TypeConverter
import com.rey.moviedb.repository.IRepository

class Converters {
    @TypeConverter
    fun fromType(type: IRepository.Type): Int {
        return when (type) {
            IRepository.Type.POPULAR -> 0
            IRepository.Type.TOP_RATED -> 1
            IRepository.Type.NOW_PLAYING -> 2
            IRepository.Type.FAVORITE -> 3
        }
    }

    @TypeConverter
    fun toType(id: Int): IRepository.Type {
        return when (id) {
            0 -> IRepository.Type.POPULAR
            1 -> IRepository.Type.TOP_RATED
            2 -> IRepository.Type.NOW_PLAYING
            3 -> IRepository.Type.FAVORITE
            else -> throw IllegalArgumentException("unknown type")
        }
    }
}
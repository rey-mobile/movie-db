package com.rey.moviedb.data.impl

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.rey.moviedb.data.IData
import com.rey.moviedb.data.api.MovieDbApi
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.Result
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteData @Inject constructor(private val api: MovieDbApi) : IData {
    override suspend fun getPopularMovies(page: Long, apiKey: String): Result<List<MovieModel>> =
        try {
            val result = api.getPopularMovies(page, apiKey).let { popularModel ->
                popularModel.results.map { movie ->
                    movie.pageIndex = popularModel.page
                }

                popularModel.results
            }
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e)
        }

    override suspend fun getTopRatedMovies(page: Long, apiKey: String): Result<List<MovieModel>> =
        try {
            val result = api.getTopRatedMovies(page, apiKey).let { topRatedModel ->
                topRatedModel.results.map { movie ->
                    movie.pageIndex = topRatedModel.page
                }

                topRatedModel.results
            }
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e)
        }

    override suspend fun getNowPlayingMovies(page: Long, apiKey: String): Result<List<MovieModel>> =
        try {
            val result = api.getNowPlayingMovies(page, apiKey).let { nowPlayingModel ->
                nowPlayingModel.results.map { movie ->
                    movie.pageIndex = nowPlayingModel.page
                }

                nowPlayingModel.results
            }
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e)
        }

    override suspend fun getMovieDetail(id: Long, apiKey: String): Result<MovieModel> =
        try {
            val result = api.getMovieDetail(id, apiKey).let {
                it.pageIndex = 0
                it
            }
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e)
        }

    override suspend fun getMovieReviews(
        id: Long, page: Long, apiKey: String
    ): Result<List<ReviewModel>> =
        try {
            val result = api.getMovieReviews(id, page, apiKey).let { reviewsModel ->
                reviewsModel.results.map {
                    it.pageIndex = reviewsModel.page
                    it.movieId = reviewsModel.id
                }

                reviewsModel.results
            }
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e)
        }

    override suspend fun insertMoviesCache(list: List<MovieModel>): List<Long> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun insertMovieReviews(list: List<ReviewModel>): List<Long> {
        TODO("Not yet implemented")
    }

    override suspend fun deleteAllMoviesCache(type: IRepository.Type) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun observeMoviesCache(type: IRepository.Type): DataSource.Factory<Int, MovieModel> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun observeMovieDetail(
        type: IRepository.Type,
        id: Long
    ): LiveData<MovieModel> {
        TODO("Not yet implemented")
    }

    override fun observeMovieReviews(id: Long): DataSource.Factory<Int, ReviewModel> {
        TODO("Not yet implemented")
    }

    override suspend fun insertFavoriteMovie(movie: MovieModel) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun deleteFavoriteMovie(
        id: Long
    ) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun observeFavoriteMovies(): DataSource.Factory<Int, MovieModel> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun observeFavoriteMovie(id: Long): LiveData<MovieModel> {
        TODO("Not yet implemented")
    }

    override suspend fun getLastPageIndex(): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
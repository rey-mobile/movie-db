package com.rey.moviedb.data.api

import com.rey.moviedb.model.*
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieDbApi {
    @GET("popular")
    suspend fun getPopularMovies(
        @Query("page") page: Long,
        @Query("api_key") apiKey: String
    ): PopularModel

    @GET("top_rated")
    suspend fun getTopRatedMovies(
        @Query("page") page: Long,
        @Query("api_key") apiKey: String
    ): TopRatedModel

    @GET("now_playing")
    suspend fun getNowPlayingMovies(
        @Query("page") page: Long,
        @Query("api_key") apiKey: String
    ): NowPlayingModel

    @GET("{id}")
    suspend fun getMovieDetail(
        @Path("id") id: Long,
        @Query("api_key") apiKey: String
    ): MovieModel

    @GET("{id}/reviews")
    suspend fun getMovieReviews(
        @Path("id") id: Long,
        @Query("page") page: Long,
        @Query("api_key") apiKey: String
    ): ReviewsModel
}
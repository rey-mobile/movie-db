package com.rey.moviedb.data.cache.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository

@Dao
interface MovieDao {
    @Insert
    suspend fun insertMovies(list: List<MovieModel>): List<Long>

    @Query("DELETE FROM MovieModel WHERE category = :type")
    suspend fun deleteAllMovies(type: IRepository.Type)

    @Query("SELECT * FROM MovieModel WHERE category = :type ORDER BY localId")
    fun getAllMovies(type: IRepository.Type): DataSource.Factory<Int, MovieModel>

    @Query("SELECT * FROM MovieModel WHERE category = :type AND id = :id")
    fun getMovieDetail(type: IRepository.Type, id: Long): LiveData<MovieModel>

    @Insert
    suspend fun insertMovieReviews(list: List<ReviewModel>): List<Long>

    @Query("SELECT * FROM ReviewModel WHERE movieId = :id")
    fun getMovieReviews(id: Long): DataSource.Factory<Int, ReviewModel>

    @Query("SELECT COALESCE(MAX(pageIndex), 0) FROM MovieModel ORDER BY localId")
    suspend fun getLastPageIndex(): Long

    @Insert
    suspend fun insertFavoriteMovie(movie: MovieModel)

    @Query("DELETE FROM MovieModel WHERE category = :type AND id = :id")
    suspend fun deleteFavoriteMovie(type: IRepository.Type, id: Long)

    @Query("SELECT * FROM MovieModel WHERE category = :type AND id = :id")
    fun getFavoriteMovieDetail(type: IRepository.Type, id: Long): LiveData<MovieModel>
}
package com.rey.moviedb.data.impl

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.rey.moviedb.data.IData
import com.rey.moviedb.data.cache.dao.MovieDao
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.Result
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalData @Inject constructor(private val dao: MovieDao) : IData {
    override suspend fun getPopularMovies(page: Long, apiKey: String): Result<List<MovieModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getTopRatedMovies(page: Long, apiKey: String): Result<List<MovieModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getNowPlayingMovies(page: Long, apiKey: String): Result<List<MovieModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getMovieDetail(id: Long, apiKey: String): Result<MovieModel> {
        TODO("Not yet implemented")
    }

    override suspend fun getMovieReviews(
        id: Long,
        page: Long,
        apiKey: String
    ): Result<List<ReviewModel>> {
        TODO("Not yet implemented")
    }

    override suspend fun insertMoviesCache(list: List<MovieModel>): List<Long> =
        dao.insertMovies(list)

    override suspend fun insertMovieReviews(list: List<ReviewModel>): List<Long> =
        dao.insertMovieReviews(list)

    override suspend fun deleteAllMoviesCache(type: IRepository.Type) = dao.deleteAllMovies(type)

    override fun observeMoviesCache(type: IRepository.Type): DataSource.Factory<Int, MovieModel> =
        dao.getAllMovies(type)

    override fun observeMovieDetail(
        type: IRepository.Type,
        id: Long
    ): LiveData<MovieModel> = dao.getMovieDetail(type, id)

    override fun observeMovieReviews(id: Long): DataSource.Factory<Int, ReviewModel> =
        dao.getMovieReviews(id)

    override suspend fun insertFavoriteMovie(movie: MovieModel) {
        val favorite = movie.copy(localId = 0)
        favorite.category = IRepository.Type.FAVORITE
        dao.insertFavoriteMovie(favorite)
    }

    override suspend fun deleteFavoriteMovie(id: Long) =
        dao.deleteFavoriteMovie(IRepository.Type.FAVORITE, id)

    override fun observeFavoriteMovies(): DataSource.Factory<Int, MovieModel> =
        dao.getAllMovies(IRepository.Type.FAVORITE)

    override fun observeFavoriteMovie(id: Long): LiveData<MovieModel> =
        dao.getFavoriteMovieDetail(IRepository.Type.FAVORITE, id)

    override suspend fun getLastPageIndex(): Long = dao.getLastPageIndex()
}
package com.rey.moviedb.util

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.google.android.material.snackbar.Snackbar
import com.rey.moviedb.R

fun View.showSnackbar(message: String, length: Int) {
    val snack = Snackbar.make(this, message, length).run {
        addCallback(object : Snackbar.Callback() {
            override fun onShown(sb: Snackbar?) {
            }

            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            }
        })
    }

    snack.setAction(R.string.ok) { snack.dismiss() }
    snack.show()
}

fun View.setupSnackbar(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<Int>>,
    length: Int
) {
    snackbarEvent.observe(lifecycleOwner, EventObserver { message ->
        showSnackbar(context.getString(message), length)
    })
}
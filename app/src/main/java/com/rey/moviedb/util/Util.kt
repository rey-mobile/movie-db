package com.rey.moviedb.util

import java.text.SimpleDateFormat
import java.util.*

fun dateFormat() = "yyyy-MM-dd"

fun getStringDateTime(date: Date = Date()): String {
    return SimpleDateFormat(dateFormat(), Locale.getDefault()).format(date)
}

fun formatStringDateToString(date: String, format: String): String {
    return SimpleDateFormat(format, Locale.getDefault()).format(
        SimpleDateFormat(
            dateFormat(),
            Locale.getDefault()
        ).parse(date)
    )
}
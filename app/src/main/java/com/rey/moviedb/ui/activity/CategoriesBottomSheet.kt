package com.rey.moviedb.ui.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rey.moviedb.R
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.ui.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.bottom_sheet_categories.*

class CategoriesBottomSheet(private val viewModel: MovieViewModel) : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        main_list_view.adapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.categories,
            android.R.layout.simple_list_item_1
        )

        main_list_view.setOnItemClickListener { _, _, selectedIndex, _ ->
            when (selectedIndex) {
                0 -> viewModel.setCategory(IRepository.Type.POPULAR)
                1 -> viewModel.setCategory(IRepository.Type.TOP_RATED)
                2 -> viewModel.setCategory(IRepository.Type.NOW_PLAYING)
            }

            this.dismiss()
        }
    }

}
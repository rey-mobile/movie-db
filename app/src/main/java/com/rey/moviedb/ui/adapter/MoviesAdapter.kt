package com.rey.moviedb.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.rey.moviedb.R
import com.rey.moviedb.databinding.MovieItemBinding
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.ui.viewmodel.MovieViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.asExecutor

class MoviesAdapter(dispatcher: CoroutineDispatcher, private val viewModel: MovieViewModel) :
    PagedAdapter<MovieModel, MovieItemBinding>(object : DiffUtil.ItemCallback<MovieModel>() {
        override fun areItemsTheSame(oldItem: MovieModel, newItem: MovieModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MovieModel, newItem: MovieModel): Boolean {
            return oldItem.posterPath == newItem.posterPath && oldItem.overview == newItem.overview && oldItem.category == newItem.category
        }
    }, dispatcher.asExecutor()) {

    override fun createBinding(parent: ViewGroup): MovieItemBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.movie_item,
            parent,
            false
        )
    }

    override fun bind(binding: MovieItemBinding, item: MovieModel) {
        binding.dateFormat = "MMM dd, yyyy"
        binding.movie = item
        binding.imageContentDescription = "${item.title}'s poster."
        binding.vm = viewModel
    }
}
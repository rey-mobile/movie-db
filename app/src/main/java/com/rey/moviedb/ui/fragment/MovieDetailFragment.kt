package com.rey.moviedb.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.rey.moviedb.BuildConfig
import com.rey.moviedb.R
import com.rey.moviedb.databinding.FragmentMovieDetailBinding
import com.rey.moviedb.di.property.Injectable
import com.rey.moviedb.ui.adapter.MovieReviewsAdapter
import com.rey.moviedb.ui.viewmodel.MovieViewModel
import com.rey.moviedb.ui.viewmodel.MovieViewModelFactory
import com.rey.moviedb.ui.viewmodel.SavedStateViewModelFactory
import com.rey.moviedb.util.setupSnackbar
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class MovieDetailFragment : Fragment(), Injectable {

    private lateinit var binding: FragmentMovieDetailBinding

    private val args: MovieDetailFragmentArgs by navArgs()

    private lateinit var adapter: MovieReviewsAdapter

    @Inject
    lateinit var dispatcher: CoroutineDispatcher

    @Inject
    lateinit var viewModelFactory: MovieViewModelFactory

    private val viewModel: MovieViewModel by activityViewModels {
        SavedStateViewModelFactory(viewModelFactory, this)
    }

    private fun setupSnackbar() {
        view?.setupSnackbar(this, viewModel.snackbarMessage, Snackbar.LENGTH_LONG)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupSnackbar()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = MovieReviewsAdapter(dispatcher)

        viewModel.movieReviews.observe(this, Observer {
            adapter.submitList(it)

            if (BuildConfig.DEBUG)
                adapter.notifyDataSetChanged()
        })

        viewModel.getMovieDetail(args.movieId)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_movie_detail, container, false)
        binding.lifecycleOwner = this
        binding.apply {
            this.vm = viewModel
            this.dateFormat = "MMM dd, yyyy"
            this.movieDetailRecyclerReviews.adapter = adapter
        }

        return binding.root
    }

}

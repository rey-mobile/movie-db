package com.rey.moviedb.ui.fragment

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.rey.moviedb.BuildConfig
import com.rey.moviedb.R
import com.rey.moviedb.databinding.FragmentMoviesBinding
import com.rey.moviedb.di.property.Injectable
import com.rey.moviedb.ui.activity.CategoriesBottomSheet
import com.rey.moviedb.ui.adapter.MoviesAdapter
import com.rey.moviedb.ui.viewmodel.MovieViewModel
import com.rey.moviedb.ui.viewmodel.MovieViewModelFactory
import com.rey.moviedb.ui.viewmodel.SavedStateViewModelFactory
import com.rey.moviedb.util.EventObserver
import kotlinx.android.synthetic.main.fragment_movies.*
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class MoviesFragment : Fragment(), Injectable {

    @Inject
    lateinit var dispatcher: CoroutineDispatcher

    @Inject
    lateinit var viewModelFactory: MovieViewModelFactory

    private lateinit var binding: FragmentMoviesBinding

    private lateinit var adapter: MoviesAdapter

    private lateinit var categoriesBottomSheet: CategoriesBottomSheet

    private val viewModel: MovieViewModel by activityViewModels {
        SavedStateViewModelFactory(viewModelFactory, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        categoriesBottomSheet = CategoriesBottomSheet(viewModel)
        adapter = MoviesAdapter(dispatcher, viewModel)

        viewModel.movies.observe(this, Observer {
            adapter.submitList(it)

            if (BuildConfig.DEBUG)
                adapter.notifyDataSetChanged()
        })

        viewModel.movieId.observe(this, EventObserver {
            findNavController().navigate(
                MoviesFragmentDirections.actionMoviesFragmentToMovieDetailFragment(
                    it
                )
            )
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movies, container, false)
        binding.lifecycleOwner = this
        binding.apply {
            moviesRecycler.adapter = adapter
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        movies_btn_categories.setOnClickListener {
            categoriesBottomSheet.show(requireActivity().supportFragmentManager, "categories")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_moviesFragment_to_favoriteMoviesFragment -> findNavController().navigate(
                MoviesFragmentDirections.actionMoviesFragmentToFavoriteMoviesFragment()
            )
        }

        return true
    }
}

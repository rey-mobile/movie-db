package com.rey.moviedb.ui.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner

class SavedStateViewModelFactory<out V : ViewModel>(
    private val assistedViewModelFactory: AssistedViewModelFactory<V>,
    owner: SavedStateRegistryOwner,
    bundle: Bundle? = null
) : AbstractSavedStateViewModelFactory(owner, bundle) {
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        @Suppress("UNCHECKED_CAST")
        return assistedViewModelFactory.create(handle) as T
    }
}
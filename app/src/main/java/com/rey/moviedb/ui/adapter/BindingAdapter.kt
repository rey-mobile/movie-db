package com.rey.moviedb.ui.adapter

import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.rey.moviedb.R
import com.rey.moviedb.util.formatStringDateToString

@BindingAdapter("app:imageFromUrl")
fun imageUrl(imageView: ImageView, url: String?) {
    if (!url.isNullOrEmpty())
        Glide.with(imageView.context).load(url)
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.ic_error_black_24dp)
            .into(imageView)
}

@BindingAdapter("app:bindDate", "app:bindDateFormat")
fun bindDate(textView: TextView, date: String?, format: String) {
    if (!date.isNullOrEmpty())
        textView.text = formatStringDateToString(date, format)
}

@BindingAdapter("app:setFavoriteIcon")
fun setFavoriteIcon(imageButton: ImageButton, isFavorite: Boolean?) {
    if (isFavorite == true) {
        imageButton.setImageDrawable(
            ContextCompat.getDrawable(
                imageButton.context,
                R.drawable.ic_favorite_black_24dp
            )
        )

        imageButton.contentDescription = imageButton.context.getString(R.string.remove_love)
    } else {
        imageButton.setImageDrawable(
            ContextCompat.getDrawable(
                imageButton.context,
                R.drawable.ic_favorite_border_black_24dp
            )
        )

        imageButton.contentDescription = imageButton.context.getString(R.string.love_movie)
    }
}
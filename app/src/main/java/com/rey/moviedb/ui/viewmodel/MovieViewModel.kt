package com.rey.moviedb.ui.viewmodel

import androidx.lifecycle.*
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import androidx.paging.PagedList
import com.rey.moviedb.R
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.Event
import com.rey.moviedb.util.wrapEspressoIdlingResource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieViewModel @Inject constructor(
    private val repository: IRepository,
    private val dispatcher: CoroutineDispatcher,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    companion object {
        const val TYPE_KEY = "moviesType"
        const val MOVIE_ID_KEY = "movieId"
    }

    init {
        if (!savedStateHandle.contains(TYPE_KEY)) {
            savedStateHandle.set(TYPE_KEY, IRepository.Type.POPULAR)
        }
    }

    private val _snackbarMessage = MutableLiveData<Event<Int>>()
    val snackbarMessage: LiveData<Event<Int>> = _snackbarMessage

    private val _movieId = MutableLiveData<Event<Long>>()
    val movieId: LiveData<Event<Long>> = _movieId

    val movie: LiveData<MovieModel> = switchMap(savedStateHandle.getLiveData<Long>(MOVIE_ID_KEY)) {
        val result = repository.observeMovie(
            savedStateHandle.get<IRepository.Type>(TYPE_KEY) ?: IRepository.Type.POPULAR, it
        )

        result
    }

    val favoriteMovies: LiveData<PagedList<MovieModel>> = repository.observeFavoriteMovies()

    val favoriteMovie: LiveData<MovieModel> =
        switchMap(savedStateHandle.getLiveData<Long>(MOVIE_ID_KEY)) {
            repository.observeFavoriteMovie(it)
        }

    val movieReviews: LiveData<PagedList<ReviewModel>> =
        savedStateHandle.getLiveData<Long>(MOVIE_ID_KEY).switchMap {
            repository.observeMovieReviews(it)
        }

    val isMovieFavorite: LiveData<Boolean> = map(favoriteMovie) {
        it?.category == IRepository.Type.FAVORITE
    }

    val movies: LiveData<PagedList<MovieModel>> =
        savedStateHandle.getLiveData<IRepository.Type>(TYPE_KEY).switchMap {
            repository.observeMovies(it)
        }

    fun refresh() {
        viewModelScope.launch(dispatcher) {
            wrapEspressoIdlingResource {
                repository.refreshMovies(
                    savedStateHandle.get<IRepository.Type>(TYPE_KEY) ?: IRepository.Type.POPULAR
                )
            }
        }
    }

    fun setCategory(type: IRepository.Type) {
        savedStateHandle.set(TYPE_KEY, type)
    }

    fun getMovieDetail(id: Long) {
        savedStateHandle.set(MOVIE_ID_KEY, id)
    }

    fun navigateToMovieDetail(id: Long) {
        _movieId.value = Event(id)
    }

    fun setFavorite() {
        viewModelScope.launch(dispatcher) {
            wrapEspressoIdlingResource {
                if (isMovieFavorite.value != true) {
                    repository.insertFavoriteMovie(movie.value!!)
                    _snackbarMessage.postValue(Event(R.string.movie_saved_to_favorite))
                } else {
                    repository.deleteFavoriteMovie(movie.value!!.id)
                    _snackbarMessage.postValue(Event(R.string.movie_removed_from_favorite))
                }
            }
        }
    }

}
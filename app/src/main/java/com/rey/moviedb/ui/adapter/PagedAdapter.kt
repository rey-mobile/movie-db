package com.rey.moviedb.ui.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import java.util.concurrent.Executor

class PagedViewHolder<out T : ViewDataBinding>(val binding: T) :
    RecyclerView.ViewHolder(binding.root)

abstract class PagedAdapter<T, VH : ViewDataBinding>(
    diffCallBack: DiffUtil.ItemCallback<T>,
    executor: Executor
) : PagedListAdapter<T, PagedViewHolder<VH>>(
    AsyncDifferConfig.Builder<T>(diffCallBack).setBackgroundThreadExecutor(executor).build()
) {

    abstract fun createBinding(parent: ViewGroup): VH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagedViewHolder<VH> {
        val binding = createBinding(parent)
        return PagedViewHolder(binding)
    }

    abstract fun bind(binding: VH, item: T)

    override fun onBindViewHolder(holder: PagedViewHolder<VH>, position: Int) {
        getItem(position)?.let { bind(holder.binding, it) }
        holder.binding.executePendingBindings()
    }
}
package com.rey.moviedb.ui.viewmodel

import androidx.lifecycle.SavedStateHandle
import com.rey.moviedb.repository.IRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class MovieViewModelFactory @Inject constructor(
    private val repository: IRepository,
    private val dispatcher: CoroutineDispatcher
) :
    AssistedViewModelFactory<MovieViewModel> {
    override fun create(handle: SavedStateHandle): MovieViewModel {
        return MovieViewModel(repository, dispatcher, handle)
    }
}
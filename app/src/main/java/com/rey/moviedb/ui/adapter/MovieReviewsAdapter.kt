package com.rey.moviedb.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.rey.moviedb.R
import com.rey.moviedb.databinding.MovieReviewItemBinding
import com.rey.moviedb.model.ReviewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.asExecutor

class MovieReviewsAdapter(dispatcher: CoroutineDispatcher) :
    PagedAdapter<ReviewModel, MovieReviewItemBinding>(object :
        DiffUtil.ItemCallback<ReviewModel>() {
        override fun areItemsTheSame(oldItem: ReviewModel, newItem: ReviewModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ReviewModel, newItem: ReviewModel): Boolean {
            return oldItem.content == newItem.content && oldItem.id == newItem.id
        }
    }, dispatcher.asExecutor()) {
    override fun createBinding(parent: ViewGroup): MovieReviewItemBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.movie_review_item,
            parent,
            false
        )
    }

    override fun bind(binding: MovieReviewItemBinding, item: ReviewModel) {
        binding.review = item
    }
}
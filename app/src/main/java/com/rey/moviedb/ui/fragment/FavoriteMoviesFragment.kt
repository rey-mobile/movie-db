package com.rey.moviedb.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.rey.moviedb.BuildConfig
import com.rey.moviedb.R
import com.rey.moviedb.databinding.FragmentFavoriteMoviesBinding
import com.rey.moviedb.di.property.Injectable
import com.rey.moviedb.ui.adapter.MoviesAdapter
import com.rey.moviedb.ui.viewmodel.MovieViewModel
import com.rey.moviedb.ui.viewmodel.MovieViewModelFactory
import com.rey.moviedb.ui.viewmodel.SavedStateViewModelFactory
import com.rey.moviedb.util.EventObserver
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class FavoriteMoviesFragment : Fragment(), Injectable {
    private lateinit var binding: FragmentFavoriteMoviesBinding
    private lateinit var adapter: MoviesAdapter

    @Inject
    lateinit var viewModelFactory: MovieViewModelFactory

    @Inject
    lateinit var dispatcher: CoroutineDispatcher

//        private lateinit var extras: Navigator.Extras
//    private val bundle: Bundle = Bundle()

    private val viewModel: MovieViewModel by activityViewModels {
        SavedStateViewModelFactory(viewModelFactory, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = MoviesAdapter(dispatcher, viewModel)

        viewModel.favoriteMovies.observe(this, Observer {
            adapter.submitList(it)

            if (BuildConfig.DEBUG)
                adapter.notifyDataSetChanged()
        })

        viewModel.movieId.observe(this, EventObserver {
            findNavController().navigate(
                FavoriteMoviesFragmentDirections.actionFavoriteMoviesFragmentToMovieDetailFragment(
                    it
                )
            )
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_favorite_movies, container, false)
        binding.lifecycleOwner = this
        binding.apply {
            favoriteRecycler.adapter = adapter
        }

        return binding.root
    }

}

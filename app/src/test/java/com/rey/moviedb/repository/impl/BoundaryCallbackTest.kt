package com.rey.moviedb.repository.impl

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.rey.moviedb.data.impl.FakeLocalData
import com.rey.moviedb.data.impl.FakeRemoteData
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class BoundaryCallbackTest {

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private lateinit var context: Context
    private lateinit var localData: FakeLocalData
    private lateinit var remoteData: FakeRemoteData
    private lateinit var boundaryCallback: BoundaryCallback

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        localData = FakeLocalData()
        remoteData = FakeRemoteData()
    }

    @Test
    fun requestAndSave_PopularMovies_ReturnSuccess() = runBlockingTest {
        val items = context.popularItems()
        remoteData.popularMovies.addAll(items)
        boundaryCallback = BoundaryCallback(
            IRepository.Type.POPULAR,
            localData,
            remoteData,
            BASE_POSTER_URL,
            API_KEY,
            Dispatchers.Main
        )

        boundaryCallback.requestAndSave()

        assertThat(localData.movies).hasSize(20)
        assertThat(localData.movies[0].id).isEqualTo(items[0].id)
    }

    @Test
    fun requestAndSave_TopRatedMovies_ReturnSuccess() = runBlockingTest {
        val items = context.topRatedItems()
        remoteData.topRatedMovies.addAll(items)
        boundaryCallback = BoundaryCallback(
            IRepository.Type.TOP_RATED,
            localData,
            remoteData,
            BASE_POSTER_URL,
            API_KEY,
            Dispatchers.Main
        )

        boundaryCallback.requestAndSave()

        assertThat(localData.movies).hasSize(20)
        assertThat(localData.movies[0].id).isEqualTo(items[0].id)
    }

    @Test
    fun requestAndSave_NowPlayingMovies_ReturnSuccess() = runBlockingTest {
        val items = context.nowPlayingItems()
        remoteData.nowPlayingMovies.addAll(items)
        boundaryCallback = BoundaryCallback(
            IRepository.Type.NOW_PLAYING,
            localData,
            remoteData,
            BASE_POSTER_URL,
            API_KEY,
            Dispatchers.Main
        )

        boundaryCallback.requestAndSave()

        assertThat(localData.movies).hasSize(20)
        assertThat(localData.movies[0].id).isEqualTo(items[0].id)
    }
}
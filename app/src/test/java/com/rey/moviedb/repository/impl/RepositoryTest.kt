package com.rey.moviedb.repository.impl

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.rey.moviedb.data.impl.FakeLocalData
import com.rey.moviedb.data.impl.FakeRemoteData
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class RepositoryTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private lateinit var context: Context
    private lateinit var localData: FakeLocalData
    private lateinit var remoteData: FakeRemoteData
    private lateinit var repository: IRepository
    private lateinit var popularItems: List<MovieModel>
    private lateinit var topRatedItems: List<MovieModel>
    private lateinit var nowPlayingItems: List<MovieModel>
    private lateinit var movieReviewsItems: List<ReviewModel>

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        localData = FakeLocalData()
        remoteData = FakeRemoteData()
        popularItems = context.popularItems()
        topRatedItems = context.topRatedItems()
        nowPlayingItems = context.nowPlayingItems()
        movieReviewsItems = context.reviewItems()

        remoteData.popularMovies.addAll(popularItems)
        remoteData.topRatedMovies.addAll(topRatedItems)
        remoteData.nowPlayingMovies.addAll(nowPlayingItems)
        localData.movies.addAll(popularItems)
        localData.movies.addAll(topRatedItems)
        localData.movies.addAll(nowPlayingItems)

        repository = Repository(localData, remoteData, Dispatchers.Main, API_KEY, BASE_POSTER_URL)
    }

    @Test
    fun insertFavoriteMovie_Success() = runBlockingTest {
        repository.insertFavoriteMovie(popularItems[0])

        assertThat(localData.movies).hasSize(61)
    }

    @Test
    fun deleteFavoriteMovie_Success() = runBlockingTest {
        val favoriteMovie = popularItems[0].copy()
        favoriteMovie.category = IRepository.Type.FAVORITE
        repository.insertFavoriteMovie(favoriteMovie)

        assertThat(localData.movies).hasSize(61)

        repository.deleteFavoriteMovie(favoriteMovie.id)

        assertThat(localData.movies).hasSize(60)
    }

    @Test
    fun observeFavoriteMovies_ReturnFavoriteMovies() = runBlockingTest {
        val favoriteMovie = popularItems[1].copy()
        favoriteMovie.category = IRepository.Type.FAVORITE
        repository.insertFavoriteMovie(favoriteMovie)

        val favoriteMovies = repository.observeFavoriteMovies().getOrAwaitValue()

        assertThat(favoriteMovies).hasSize(1)
        assertThat(favoriteMovies).contains(favoriteMovie)
    }

    @Test
    fun observeFavoriteMovieDetail_FromPopularMovies_ReturnFavoriteMovieDetail() =
        mainCoroutineRule.runBlockingTest {
            val favoriteMovie = popularItems[0].copy()
            favoriteMovie.category = IRepository.Type.FAVORITE
            repository.insertFavoriteMovie(favoriteMovie)
            val movie = repository.observeFavoriteMovie(475303).getOrAwaitValue()

            assertThat(movie.id).isEqualTo(475303)
            assertThat(movie.category).isEqualTo(IRepository.Type.FAVORITE)
        }

    @Test
    fun observeMovieDetail_FromPopularMovies_ReturnFromNetwork() =
        mainCoroutineRule.runBlockingTest {
            val movie = repository.observeMovie(IRepository.Type.POPULAR, 475303).getOrAwaitValue()

            assertThat(movie.id).isEqualTo(475303)
            assertThat(movie.posterPath).contains("fakeUrl")
        }

    @Test
    fun observeMovieDetail_FromPopularMovies_ReturnFromLocal() = mainCoroutineRule.runBlockingTest {
        remoteData.setError(true)
        val movie = repository.observeMovie(IRepository.Type.POPULAR, 475303).getOrAwaitValue()

        assertThat(movie.id).isEqualTo(475303)
        assertThat(movie.posterPath).doesNotContain("fakeUrl")
    }

    @Test
    fun observeMoviesCache_GetPopularMovies_ReturnMovies() = mainCoroutineRule.runBlockingTest {
        val movies = repository.observeMovies(IRepository.Type.POPULAR).getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(popularItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(popularItems[popularItems.lastIndex].id)
    }

    @Test
    fun observeMoviesCache_GetTopRatedMovies_ReturnMovies() = mainCoroutineRule.runBlockingTest {
        val movies = repository.observeMovies(IRepository.Type.TOP_RATED).getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(topRatedItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(topRatedItems[topRatedItems.lastIndex].id)
    }

    @Test
    fun observeMoviesCache_GetNowPlayingMovies_ReturnMovies() = mainCoroutineRule.runBlockingTest {
        val movies = repository.observeMovies(IRepository.Type.NOW_PLAYING).getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(nowPlayingItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(nowPlayingItems[nowPlayingItems.lastIndex].id)
    }

    @Test
    fun refreshMovies_PopularMovies_PopularMoviesCached() = mainCoroutineRule.runBlockingTest {
        var popular =
            localData.movies.filter { movieModel -> movieModel.category == IRepository.Type.POPULAR }
                .toMutableList()
        popular.removeAt(popular.lastIndex)

        assertThat(popular).hasSize(19)
        assertThat(popular[0].id).isEqualTo(popularItems[0].id)
        assertThat(popular[popular.lastIndex].id).isEqualTo(popularItems[popularItems.lastIndex - 1].id)

        repository.refreshMovies(IRepository.Type.POPULAR)

        popular =
            localData.movies.filter { movieModel -> movieModel.category == IRepository.Type.POPULAR }
                .toMutableList()

        assertThat(popular).hasSize(20)
        assertThat(popular[0].id).isEqualTo(popularItems[0].id)
        assertThat(popular[popular.lastIndex].id).isEqualTo(popularItems[popularItems.lastIndex].id)
    }

    @Test
    fun refreshMovies_TopRatedMovies_TopRatedCached() = mainCoroutineRule.runBlockingTest {
        var topRated =
            localData.movies.filter { movieModel -> movieModel.category == IRepository.Type.TOP_RATED }
                .toMutableList()
        topRated.removeAt(topRated.lastIndex)

        assertThat(topRated).hasSize(19)
        assertThat(topRated[0].id).isEqualTo(topRatedItems[0].id)
        assertThat(topRated[topRated.lastIndex].id).isEqualTo(topRatedItems[topRatedItems.lastIndex - 1].id)

        repository.refreshMovies(IRepository.Type.TOP_RATED)

        topRated =
            localData.movies.filter { movieModel -> movieModel.category == IRepository.Type.TOP_RATED }
                .toMutableList()

        assertThat(topRated).hasSize(20)
        assertThat(topRated[0].id).isEqualTo(topRatedItems[0].id)
        assertThat(topRated[topRated.lastIndex].id).isEqualTo(topRatedItems[topRatedItems.lastIndex].id)
    }

    @Test
    fun refreshMovies_NowPlayingMovies_NowPlayingCached() = mainCoroutineRule.runBlockingTest {
        var nowPlaying =
            localData.movies.filter { movieModel -> movieModel.category == IRepository.Type.NOW_PLAYING }
                .toMutableList()
        nowPlaying.removeAt(nowPlaying.lastIndex)

        assertThat(nowPlaying).hasSize(19)
        assertThat(nowPlaying[0].id).isEqualTo(nowPlayingItems[0].id)
        assertThat(nowPlaying[nowPlaying.lastIndex].id).isEqualTo(nowPlayingItems[nowPlayingItems.lastIndex - 1].id)

        repository.refreshMovies(IRepository.Type.TOP_RATED)

        nowPlaying =
            localData.movies.filter { movieModel -> movieModel.category == IRepository.Type.NOW_PLAYING }
                .toMutableList()

        assertThat(nowPlaying).hasSize(20)
        assertThat(nowPlaying[0].id).isEqualTo(nowPlayingItems[0].id)
        assertThat(nowPlaying[nowPlaying.lastIndex].id).isEqualTo(nowPlayingItems[nowPlayingItems.lastIndex].id)
    }

    @Test
    fun observeReviews_ReturnReviews() = mainCoroutineRule.runBlockingTest {
        remoteData.movieReviews.addAll(movieReviewsItems)
        val movie =
            repository.observeMovie(popularItems[1].category, popularItems[1].id).getOrAwaitValue()

        assertThat(movie).isNotNull()

        assertThat(localData.movieReviews).isNotEmpty()

        var result = repository.observeMovieReviews(movie.id).getOrAwaitValue()

        assertThat(result).hasSize(2)

        var content = result.filter { it.movieId == movie.id }
        assertThat(content).isNotEmpty()

        result = repository.observeMovieReviews(popularItems[3].id).getOrAwaitValue()

        assertThat(result).hasSize(7)

        content = result.filter { it.movieId == popularItems[3].id }
        assertThat(content).isNotEmpty()
    }
}
package com.rey.moviedb.ui.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.rey.moviedb.R
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.FakeRepository
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.ui.viewmodel.MovieViewModel.Companion.MOVIE_ID_KEY
import com.rey.moviedb.ui.viewmodel.MovieViewModel.Companion.TYPE_KEY
import com.rey.moviedb.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before

import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class MovieViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private lateinit var context: Context
    private lateinit var repository: FakeRepository
    private lateinit var savedStateHandle: SavedStateHandle
    private lateinit var viewModel: MovieViewModel
    private lateinit var popularItems: List<MovieModel>
    private lateinit var topRatedItems: List<MovieModel>
    private lateinit var nowPlayingItems: List<MovieModel>
    private lateinit var favoriteItems: List<MovieModel>
    private lateinit var movieReviewItems: List<ReviewModel>

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        repository = FakeRepository()
        savedStateHandle = SavedStateHandle()

        popularItems = context.popularItems()
        topRatedItems = context.topRatedItems()
        nowPlayingItems = context.nowPlayingItems()
        favoriteItems = context.favoriteItems()
        movieReviewItems = context.reviewItems()

        viewModel = MovieViewModel(repository, Dispatchers.Main, savedStateHandle)
    }

    @Test
    fun movies_InitialPopular_ReturnPopularMovies() = runBlockingTest {
        repository.moviesLive.postValue(popularItems)

        savedStateHandle.set(TYPE_KEY, IRepository.Type.POPULAR)
        viewModel = MovieViewModel(repository, Dispatchers.Main, savedStateHandle)

        val movies = viewModel.movies.getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(popularItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(popularItems[popularItems.lastIndex].id)
    }

    @Test
    fun movies_InitialTopRated_ReturnTopRatedMovies() = runBlockingTest {
        repository.moviesLive.postValue(topRatedItems)

        savedStateHandle.set(TYPE_KEY, IRepository.Type.TOP_RATED)
        viewModel = MovieViewModel(repository, Dispatchers.Main, savedStateHandle)

        val movies = viewModel.movies.getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(topRatedItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(topRatedItems[topRatedItems.lastIndex].id)
    }

    @Test
    fun movies_InitialNowPlaying_ReturnNowPlayingMovies() = runBlockingTest {
        repository.moviesLive.postValue(nowPlayingItems)

        savedStateHandle.set(TYPE_KEY, IRepository.Type.NOW_PLAYING)
        viewModel = MovieViewModel(repository, Dispatchers.Main, savedStateHandle)

        val movies = viewModel.movies.getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(nowPlayingItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(nowPlayingItems[nowPlayingItems.lastIndex].id)
    }

    @Test
    fun movies_RefreshMovies_CacheRefreshed() = mainCoroutineRule.runBlockingTest {
        savedStateHandle.set(TYPE_KEY, IRepository.Type.POPULAR)
        viewModel = MovieViewModel(repository, Dispatchers.Main, savedStateHandle)

        repository.popularMovies.addAll(popularItems)
        repository.moviesLive.postValue(emptyList())

        assertThat(viewModel.movies.getOrAwaitValue()).hasSize(0)

        viewModel.refresh()

        assertThat(viewModel.movies.getOrAwaitValue()).hasSize(20)
    }

    @Test
    fun movies_ChangeCategoryFromTopRatedToPopular_PopularMoviesObservedSaved() = runBlockingTest {
        savedStateHandle.set(TYPE_KEY, IRepository.Type.TOP_RATED)

        val movieList: MutableList<MovieModel> = mutableListOf()
        movieList.addAll(popularItems)
        movieList.addAll(topRatedItems)
        repository.moviesLive.postValue(movieList)

        var movies = viewModel.movies.getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(topRatedItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(topRatedItems[topRatedItems.lastIndex].id)

        viewModel.setCategory(IRepository.Type.POPULAR)

        assertThat(savedStateHandle.get<IRepository.Type>(TYPE_KEY)).isEqualTo(IRepository.Type.POPULAR)

        movies = viewModel.movies.getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(popularItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(popularItems[popularItems.lastIndex].id)
    }

    @Test
    fun favoriteMovies_observeFavoriteMovies_ReturnFavoriteMovies() = runBlockingTest {
        viewModel = MovieViewModel(repository, Dispatchers.Main, savedStateHandle)

        repository.favoriteMoviesLive.postValue(favoriteItems)

        val movies = viewModel.favoriteMovies.getOrAwaitValue()

        assertThat(movies).hasSize(20)
        assertThat(movies[0]!!.id).isEqualTo(favoriteItems[0].id)
        assertThat(movies[movies.lastIndex]!!.id).isEqualTo(favoriteItems[favoriteItems.lastIndex].id)
    }

    @Test
    fun favoriteMovies_insertNew_FavoriteMoviesAdded() = runBlockingTest {
        savedStateHandle.set(MOVIE_ID_KEY, popularItems[0].id)

        repository.moviesLive.postValue(popularItems)

        viewModel.getMovieDetail(popularItems[0].id)

        val movie = viewModel.movie.getOrAwaitValue()

        assertThat(movie.id).isEqualTo(popularItems[0].id)

        viewModel.setFavorite()

        assertThat(
            viewModel.snackbarMessage.getOrAwaitValue().getContentIfNotHandled()
        ).isEqualTo(R.string.movie_saved_to_favorite)

        val isMovieFavorite = viewModel.isMovieFavorite.getOrAwaitValue()

        assertThat(isMovieFavorite).isTrue()
    }

    @Test
    fun favoriteMovies_deleteCurrent_FavoriteMoviesAdded() = runBlockingTest {
        savedStateHandle.set(MOVIE_ID_KEY, popularItems[0].id)

        repository.moviesLive.postValue(popularItems)
        repository.favoriteMoviesLive.postValue(favoriteItems)

        viewModel.getMovieDetail(favoriteItems[0].id)

        val movie = viewModel.movie.getOrAwaitValue()

        assertThat(movie.id).isEqualTo(favoriteItems[0].id)

        var isMovieFavorite = viewModel.isMovieFavorite.getOrAwaitValue()

        assertThat(isMovieFavorite).isTrue()

        viewModel.setFavorite()

        assertThat(
            viewModel.snackbarMessage.getOrAwaitValue().getContentIfNotHandled()
        ).isEqualTo(R.string.movie_removed_from_favorite)

        isMovieFavorite = viewModel.isMovieFavorite.getOrAwaitValue()

        assertThat(isMovieFavorite).isFalse()
    }

    @Test
    fun getMovie_ReturnMovieDetail() = runBlockingTest {
        savedStateHandle.set(MOVIE_ID_KEY, popularItems[0].id)

        repository.moviesLive.postValue(popularItems)

        viewModel.getMovieDetail(popularItems[0].id)

        val movie = viewModel.movie.getOrAwaitValue()

        assertThat(movie.id).isEqualTo(popularItems[0].id)
    }

    @Test
    fun getMovie_ReturnFavoriteStatus() = runBlockingTest {
        repository.moviesLive.postValue(popularItems)
        repository.favoriteMoviesLive.postValue(favoriteItems)

        viewModel.getMovieDetail(favoriteItems[0].id)

        var isFavorite = viewModel.isMovieFavorite.getOrAwaitValue()

        assertThat(isFavorite).isTrue()

        viewModel.getMovieDetail(popularItems[0].id)

        isFavorite = viewModel.isMovieFavorite.getOrAwaitValue()

        assertThat(isFavorite).isFalse()
    }

    @Test
    fun getMovieReviews_GetMovieDetail_ReturnMovieReviews() = runBlockingTest {
        repository.movieReviewsLive.postValue(movieReviewItems)

        viewModel.getMovieDetail(popularItems[3].id)

        val reviews = viewModel.movieReviews.getOrAwaitValue()

        assertThat(reviews).hasSize(7)
        assertThat(reviews[0]?.movieId).isEqualTo(popularItems[3].id)
    }
}
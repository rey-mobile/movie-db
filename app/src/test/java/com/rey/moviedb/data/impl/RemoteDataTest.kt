package com.rey.moviedb.data.impl

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.rey.moviedb.R
import com.rey.moviedb.data.api.MovieDbApi
import com.rey.moviedb.util.API_KEY
import com.rey.moviedb.util.Result
import com.rey.moviedb.util.WEIRD_API_KEY
import com.rey.moviedb.util.succeeded
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class RemoteDataTest {

    private lateinit var context: Context
    private lateinit var server: MockWebServer
    private lateinit var api: MovieDbApi
    private lateinit var remote: RemoteData
    private lateinit var errorBody: String

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        server = MockWebServer()
        api = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
            .baseUrl(server.url("")).build().create(MovieDbApi::class.java)
        remote = RemoteData(api)

        errorBody = context.resources.openRawResource(R.raw.response_401).bufferedReader()
            .use { it.readText() }
    }

    @Test
    fun getPopularMovies_Success() = runBlocking {
        val page = 1L
        val body = context.resources.openRawResource(R.raw.popular_200).bufferedReader()
            .use { it.readText() }
        server.enqueue(MockResponse().setResponseCode(200).setBody(body))

        val result = remote.getPopularMovies(page, API_KEY).run {
            assertThat(this.succeeded).isTrue()
            (this as Result.Success).data
        }

        assertThat(result).hasSize(20)
        assertThat(result[result.lastIndex].pageIndex).isEqualTo(page)
    }

    @Test
    fun getPopularMovies_WrongApiKey_ReturnError() = runBlocking {
        server.enqueue(MockResponse().setResponseCode(401).setBody(errorBody))

        val result = remote.getPopularMovies(1, WEIRD_API_KEY)

        assertThat(result.succeeded).isFalse()
        assertThat((result as Result.Error).exception).hasMessageThat().contains("401")
    }

    @Test
    fun getTopRatedMovies_ReturnSuccess() = runBlocking {
        val page = 1L
        val body = context.resources.openRawResource(R.raw.top_rated_200).bufferedReader()
            .use { it.readText() }
        server.enqueue(MockResponse().setResponseCode(200).setBody(body))

        val result = remote.getTopRatedMovies(1, API_KEY).run {
            assertThat(this.succeeded).isTrue()
            (this as Result.Success).data
        }

        assertThat(result).hasSize(20)
        assertThat(result[result.lastIndex].pageIndex).isEqualTo(page)
    }

    @Test
    fun getTopRatedMovies_WrongApiKey_ReturnError() = runBlocking {
        server.enqueue(MockResponse().setResponseCode(401).setBody(errorBody))

        val result = remote.getTopRatedMovies(1, WEIRD_API_KEY)

        assertThat(result.succeeded).isFalse()
        assertThat((result as Result.Error).exception).hasMessageThat().contains("401")
    }

    @Test
    fun getNowPlayingMovies_ReturnSuccess() = runBlocking {
        val page = 1L
        val body = context.resources.openRawResource(R.raw.now_playing_200).bufferedReader()
            .use { it.readText() }
        server.enqueue(MockResponse().setResponseCode(200).setBody(body))

        val result = remote.getNowPlayingMovies(1, API_KEY).run {
            assertThat(this.succeeded).isTrue()
            (this as Result.Success).data
        }

        assertThat(result).hasSize(20)
        assertThat(result[result.lastIndex].pageIndex).isEqualTo(page)
    }

    @Test
    fun getNowPlayingMovies_WrongApiKey_ReturnError() = runBlocking {
        server.enqueue(MockResponse().setResponseCode(401).setBody(errorBody))

        val result = remote.getNowPlayingMovies(1, WEIRD_API_KEY)

        assertThat(result.succeeded).isFalse()
        assertThat((result as Result.Error).exception).hasMessageThat().contains("401")
    }

    @Test
    fun getMovieDetail_ReturnMovieDetail() = runBlocking {
        val body = context.resources.openRawResource(R.raw.movie_detail_200).bufferedReader()
            .use { it.readText() }
        server.enqueue(MockResponse().setResponseCode(200).setBody(body))

        val result = remote.getMovieDetail(550, API_KEY).run {
            assertThat(this.succeeded).isTrue()
            (this as Result.Success).data
        }

        assertThat(result.id).isEqualTo(550)
    }

    @Test
    fun getMovieDetail_WrongApiKey_ReturnError() = runBlocking {
        server.enqueue(MockResponse().setResponseCode(401).setBody(errorBody))

        val result = remote.getMovieDetail(550, WEIRD_API_KEY)

        assertThat(result.succeeded).isFalse()
        assertThat((result as Result.Error).exception).hasMessageThat().contains("401")
    }

    @Test
    fun getMovieReviews_ReturnMovieReviews() = runBlocking {
        val body = context.resources.openRawResource(R.raw.reviews_512200_200).bufferedReader()
            .use { it.readText() }
        server.enqueue(MockResponse().setResponseCode(200).setBody(body))

        val result = remote.getMovieReviews(512200, 1, API_KEY).run {
            assertThat(this.succeeded).isTrue()
            (this as Result.Success).data
        }

        assertThat(result).hasSize(2)
        assertThat(result[result.lastIndex].author).isEqualTo("NickFriday2005")
    }
}
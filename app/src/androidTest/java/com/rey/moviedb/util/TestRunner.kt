package com.rey.moviedb.util

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import com.rey.moviedb.di.FakeMovieDbApp

class TestRunner : AndroidJUnitRunner() {
    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, FakeMovieDbApp::class.java.name, context)
    }
}
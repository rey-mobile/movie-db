package com.rey.moviedb.ui.activity

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.rey.moviedb.R
import com.rey.moviedb.databinding.MovieItemBinding
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.ui.adapter.PagedViewHolder
import com.rey.moviedb.util.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private lateinit var context: Context
    private lateinit var activityScenario: ActivityScenario<MainActivity>
    private lateinit var popularItems: List<MovieModel>
    private lateinit var topRatedItems: List<MovieModel>
    private lateinit var favoriteItems: List<MovieModel>
    private lateinit var navController: NavController


    private fun launchActivity() {
        activityScenario = ActivityScenario.launch(MainActivity::class.java)
        activityScenario.onActivity {
            navController = it.findNavController(R.id.main_fragment)
        }
    }

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()

        popularItems = context.popularItems()
        topRatedItems = context.topRatedItems()
        favoriteItems = context.favoriteItems()
    }

    @Test
    @Ignore("flaky test - please run individually")
    fun initialLaunch_PopularMoviesToTopRatedMovies_ShowTopRatedMovies() =
        mainCoroutineRule.runBlockingTest {
            launchActivity()

            mainCoroutineRule.advanceUntilIdle()

            onView(withText(popularItems[0].title)).check(matches(isDisplayed()))
            onView(
                withText(
                    formatStringDateToString(
                        popularItems[0].releaseDate,
                        "MMM dd, yyyy"
                    )
                )
            ).check(
                matches(isDisplayed())
            )
            onView(withText(popularItems[0].overview)).check(matches(isDisplayed()))
            onView(withContentDescription("${popularItems[0].title}'s poster.")).check(
                matches(
                    isDisplayed()
                )
            )

            onView(withText(R.string.app_name)).check(matches(isDisplayed()))
            onView(withText(R.string.categories)).check(matches(isDisplayed()))
            onView(withText(R.string.categories)).perform(click())

            onView(withText("Top Rated")).perform(click())

            onView(withId(R.id.movies_recycler)).perform(
                RecyclerViewActions.scrollToPosition<PagedViewHolder<MovieItemBinding>>(
                    0
                )
            )
            onView(withText(topRatedItems[0].title)).check(matches(isDisplayed()))
            onView(
                withText(
                    formatStringDateToString(
                        topRatedItems[0].releaseDate,
                        "MMM dd, yyyy"
                    )
                )
            ).check(
                matches(isDisplayed())
            )
            onView(withText(topRatedItems[0].overview)).check(matches(isDisplayed()))
            onView(withContentDescription("${topRatedItems[0].title}'s poster.")).check(
                matches(
                    isDisplayed()
                )
            )
        }

    @Test
    fun initialLaunch_ClickFavoriteMenu_NavigateToFavoriteFragment() =
        mainCoroutineRule.runBlockingTest {
            launchActivity()

            onView(withContentDescription(R.string.favorite)).perform(click())

            assertThat(navController.currentDestination?.id).isEqualTo(R.id.favoriteMoviesFragment)
        }

    @Test
    @Ignore("flaky test - please run individually")
    fun initialLaunch_ClickFavoriteMenuClickMovie_NavigateToMovieDetailFragment() =
        mainCoroutineRule.runBlockingTest {
            launchActivity()

            onView(withContentDescription(R.string.favorite)).perform(click())

            assertThat(navController.currentDestination?.id).isEqualTo(R.id.favoriteMoviesFragment)

            onView(withId(R.id.favorite_recycler)).perform(
                RecyclerViewActions.actionOnItemAtPosition<PagedViewHolder<MovieItemBinding>>(
                    17,
                    click()
                )
            )
            onView(withText(favoriteItems[17].title)).perform(click())
        }

    @Test
    fun initialLaunch_ClickMovie_NavigateToMovieDetailFragment() =
        mainCoroutineRule.runBlockingTest {
            launchActivity()

            onView(withId(R.id.movies_recycler)).perform(
                RecyclerViewActions.scrollToPosition<PagedViewHolder<MovieItemBinding>>(20)
            )
            onView(withId(R.id.movies_recycler)).perform(
                RecyclerViewActions.scrollToPosition<PagedViewHolder<MovieItemBinding>>(0)
            )
            onView(withText(popularItems[0].title)).check(matches(isDisplayed()))
            onView(withId(R.id.movies_recycler)).perform(
                RecyclerViewActions.actionOnItemAtPosition<PagedViewHolder<MovieItemBinding>>(
                    0,
                    click()
                )
            )


            assertThat(navController.currentDestination?.id).isEqualTo(R.id.movieDetailFragment)
        }
}
package com.rey.moviedb.ui.fragment

import android.content.Context
import android.os.Bundle
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.rey.moviedb.R
import com.rey.moviedb.databinding.FragmentFavoriteMoviesBinding
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.ui.adapter.PagedViewHolder
import com.rey.moviedb.util.DataBindingIdlingResourceRule
import com.rey.moviedb.util.MainCoroutineRule
import com.rey.moviedb.util.favoriteItems
import com.rey.moviedb.util.formatStringDateToString
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@MediumTest
@RunWith(AndroidJUnit4::class)
class FavoriteMoviesFragmentTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule()

    private lateinit var context: Context
    private lateinit var scenario: FragmentScenario<FavoriteMoviesFragment>
    private lateinit var navController: TestNavHostController
    private lateinit var favoriteItems: List<MovieModel>

    private fun launchFragment() {
        scenario = launchFragmentInContainer(Bundle(), R.style.AppTheme)
        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        dataBindingIdlingResourceRule.monitorFragment(scenario)
    }

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        navController = TestNavHostController(context)
        navController.setGraph(R.navigation.navigation_main)

        favoriteItems = context.favoriteItems()
    }

    @Test
    fun initialLaunch_ShowFavoriteMovies() = mainCoroutineRule.runBlockingTest {
        launchFragment()

        onView(withId(R.id.favorite_recycler)).perform(
            RecyclerViewActions.scrollToPosition<PagedViewHolder<FragmentFavoriteMoviesBinding>>(
                17
            )
        )

        onView(withText(favoriteItems[17].title)).check(matches(isDisplayed()))
        onView(
            withText(
                formatStringDateToString(
                    favoriteItems[17].releaseDate,
                    "MMM dd, yyyy"
                )
            )
        ).check(
            matches(isDisplayed())
        )
        onView(withText(favoriteItems[17].overview)).check(matches(isDisplayed()))
        onView(withContentDescription("${favoriteItems[17].title}'s poster.")).check(
            matches(
                isDisplayed()
            )
        )
    }
}
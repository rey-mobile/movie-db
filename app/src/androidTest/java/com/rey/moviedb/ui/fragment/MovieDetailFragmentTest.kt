package com.rey.moviedb.ui.fragment

import android.content.Context
import android.os.Bundle
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.rey.moviedb.R
import com.rey.moviedb.databinding.MovieReviewItemBinding
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.ui.adapter.PagedViewHolder
import com.rey.moviedb.util.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@MediumTest
@RunWith(AndroidJUnit4::class)
class MovieDetailFragmentTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule()

    private lateinit var context: Context
    private lateinit var scenario: FragmentScenario<MovieDetailFragment>
    private lateinit var navController: TestNavHostController
    private lateinit var bundle: Bundle
    private lateinit var popularItem: MovieModel
    private lateinit var movieReviewItems: List<ReviewModel>

    private fun launchFragment(bundle: Bundle) {
        scenario = launchFragmentInContainer(bundle, R.style.AppTheme)
        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        dataBindingIdlingResourceRule.monitorFragment(scenario)
    }

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        navController = TestNavHostController(context)
        navController.setGraph(R.navigation.navigation_main)

        popularItem = context.popularItems()[3]
        movieReviewItems = context.reviewItems(popularItem.id)
        bundle = MovieDetailFragmentArgs(popularItem.id).toBundle()
    }

    @Test
    @Ignore("flaky test - please run individually")
    fun initialTest_ShowMovieDetail() = mainCoroutineRule.runBlockingTest {
        launchFragment(bundle)

        onView(withText(popularItem.title)).check(matches(isDisplayed()))
        onView(
            withText(
                formatStringDateToString(
                    popularItem.releaseDate,
                    "MMM dd, yyyy"
                )
            )
        ).check(
            matches(isDisplayed())
        )
        onView(withText(popularItem.overview)).check(matches(isDisplayed()))
        onView(withContentDescription(popularItem.title)).check(
            matches(
                isDisplayed()
            )
        )
        onView(withContentDescription(R.string.love_movie)).check(matches(isDisplayed()))
    }

    @Test
    @Ignore("flaky test - please run individually")
    fun initialLaunch_SetFavorite_ButtonChanged() = mainCoroutineRule.runBlockingTest {
        launchFragment(bundle)

        onView(withContentDescription(R.string.love_movie)).check(matches(isDisplayed()))
        onView(withContentDescription(R.string.love_movie)).perform(ViewActions.click())
        onView(withContentDescription(R.string.remove_love)).check(matches(isDisplayed()))
        onView(withContentDescription(R.string.remove_love)).perform(ViewActions.click())
        onView(withContentDescription(R.string.love_movie)).check(matches(isDisplayed()))
    }

    @Test
    fun initialLaunch_ReviewsShown() = mainCoroutineRule.runBlockingTest {
        launchFragment(bundle)

        onView(withId(R.id.movie_detail_recycler_reviews)).perform(
            RecyclerViewActions.scrollToPosition<PagedViewHolder<MovieReviewItemBinding>>(
                0
            )
        )
        onView(withText(movieReviewItems[0].author)).check(matches(isDisplayed()))
    }
}
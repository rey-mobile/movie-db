package com.rey.moviedb.data.impl

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.toLiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.rey.moviedb.data.cache.MovieDbCache
import com.rey.moviedb.data.cache.dao.MovieDao
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.getOrAwaitValue
import com.rey.moviedb.util.popularItems
import com.rey.moviedb.util.reviewItems
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class LocalDataTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var context: Context
    private lateinit var dao: MovieDao
    private lateinit var db: MovieDbCache
    private lateinit var local: LocalData
    private lateinit var popularItems: List<MovieModel>
    private lateinit var reviewItems: List<ReviewModel>

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        db = Room.inMemoryDatabaseBuilder(context, MovieDbCache::class.java).build()
        dao = db.getMovieDao()
        local = LocalData(dao)
        popularItems = context.popularItems()
        reviewItems = context.reviewItems()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun insertMoviesCache_ThanObserveAndDeleteAll_DatabaseEmpty() = runBlockingTest {
        local.insertMoviesCache(popularItems)

        var result = local.observeMoviesCache(IRepository.Type.POPULAR).toLiveData(pageSize = 20)
            .getOrAwaitValue()

        assertThat(result).hasSize(20)
        assertThat(result[0]?.id).isEqualTo(popularItems[0].id)
        assertThat(result[result.size - 1]?.id).isEqualTo(popularItems[popularItems.size - 1].id)

        local.deleteAllMoviesCache(IRepository.Type.POPULAR)

        result = local.observeMoviesCache(IRepository.Type.POPULAR).toLiveData(pageSize = 20)
            .getOrAwaitValue()

        assertThat(result).hasSize(0)
    }

    @Test
    fun insertFavoriteMovie_ThanObserveAndDelete_DatabaseEmpty() = runBlockingTest {
        local.insertFavoriteMovie(popularItems[0])

        var result = local.observeFavoriteMovies().toLiveData(pageSize = 20).getOrAwaitValue()

        assertThat(result).hasSize(1)
        assertThat(result[0]?.id).isEqualTo(popularItems[0].id)

        local.deleteFavoriteMovie(popularItems[0].id)

        result = local.observeFavoriteMovies().toLiveData(pageSize = 20).getOrAwaitValue()

        assertThat(result).hasSize(0)
    }

    @Test
    fun getLastIndexPage_ReturnLastIndexPage() = runBlockingTest {
        val items = popularItems
        local.insertMoviesCache(items)

        val result = local.observeMoviesCache(IRepository.Type.POPULAR).toLiveData(pageSize = 20)
            .getOrAwaitValue()

        assertThat(result).hasSize(20)
        assertThat(result[result.size - 1]?.id).isEqualTo(items[items.size - 1].id)

        val lastIndex = local.getLastPageIndex()
        assertThat(lastIndex).isEqualTo(1L)
    }

    @Test
    fun getMovieDetail_FromPopularMovies_ReturnMovieDetail() = runBlockingTest {
        local.insertMoviesCache(popularItems)

        val result = local.observeMovieDetail(IRepository.Type.POPULAR, 512200).getOrAwaitValue()

        assertThat(result.id).isEqualTo(512200)
        assertThat(result.category).isEqualTo(IRepository.Type.POPULAR)
    }

    @Test
    fun getFavoriteMovieDetail_FromPopularMovies_ReturnFavoriteMovie() = runBlockingTest {
        local.insertFavoriteMovie(popularItems[0])

        val result = local.observeFavoriteMovie(475303).getOrAwaitValue()

        assertThat(result.id).isEqualTo(475303)
        assertThat(result.category).isEqualTo(IRepository.Type.FAVORITE)
    }

    @Test
    fun getMovieReviews_ReturnMovieReviews() = runBlockingTest {
        local.insertMovieReviews(reviewItems)

        val result = local.observeMovieReviews(512200).toLiveData(pageSize = 20)
            .getOrAwaitValue()

        assertThat(result).hasSize(2)
        assertThat(result[result.lastIndex]?.movieId).isEqualTo(512200)
        assertThat(result[result.lastIndex]?.pageIndex).isEqualTo(1)
    }
}
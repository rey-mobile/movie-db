package com.rey.moviedb.di.module

import android.content.Context
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.repository.FakeRepository
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.favoriteItems
import com.rey.moviedb.util.popularItems
import com.rey.moviedb.util.reviewItems
import com.rey.moviedb.util.topRatedItems
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FakeRepositoryModule {
    @Singleton
    @Provides
    fun bindRepository(fakeRepository: FakeRepository, context: Context): IRepository {
        val movies: MutableList<MovieModel> = mutableListOf()
        movies.addAll(context.popularItems())
        movies.addAll(context.topRatedItems())
        movies.addAll(context.favoriteItems())

        fakeRepository.moviesLive.postValue(movies)
        fakeRepository.favoriteMoviesLive.postValue(context.favoriteItems())
        fakeRepository.movieReviewsLive.postValue(context.reviewItems())

        return fakeRepository
    }
}
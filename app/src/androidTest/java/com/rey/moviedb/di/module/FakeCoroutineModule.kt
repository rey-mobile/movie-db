package com.rey.moviedb.di.module

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
class FakeCoroutineModule {
    @Provides
    fun provideIoDispatcher(): CoroutineDispatcher {
        return Dispatchers.Main
    }
}
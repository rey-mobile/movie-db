package com.rey.moviedb.data.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.rey.moviedb.data.IData
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.FakeDataSource
import com.rey.moviedb.util.Result

class FakeLocalData : IData {

    val movies: MutableList<MovieModel> = mutableListOf()
    val movieReviews: MutableList<ReviewModel> = mutableListOf()

    override suspend fun getPopularMovies(page: Long, apiKey: String): Result<List<MovieModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getTopRatedMovies(page: Long, apiKey: String): Result<List<MovieModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getNowPlayingMovies(page: Long, apiKey: String): Result<List<MovieModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getMovieDetail(id: Long, apiKey: String): Result<MovieModel> {
        TODO("Not yet implemented")
    }

    override suspend fun getMovieReviews(
        id: Long,
        page: Long,
        apiKey: String
    ): Result<List<ReviewModel>> {
        TODO("Not yet implemented")
    }

    override suspend fun insertMoviesCache(list: List<MovieModel>): List<Long> {
        movies.addAll(list)
        return emptyList()
    }

    override suspend fun insertMovieReviews(list: List<ReviewModel>): List<Long> {
        movieReviews.addAll(list)
        return listOf()
    }

    override suspend fun deleteAllMoviesCache(type: IRepository.Type) {
        movies.removeIf { movieModel -> movieModel.category == type }
    }

    override fun observeMoviesCache(type: IRepository.Type): DataSource.Factory<Int, MovieModel> {
        val result = movies.filter { movieModel -> movieModel.category == type }
        return FakeDataSource(result)
    }

    override fun observeMovieDetail(
        type: IRepository.Type,
        id: Long
    ): LiveData<MovieModel> {
        val result = MutableLiveData<MovieModel>()
        result.value =
            movies.filter { movieModel -> movieModel.category == type && movieModel.id == id }[0]
        return result
    }

    override fun observeMovieReviews(id: Long): DataSource.Factory<Int, ReviewModel> {
        val result = movieReviews.filter { rev -> rev.movieId == id }
        return FakeDataSource(result)
    }

    override suspend fun insertFavoriteMovie(movie: MovieModel) {
        movies.add(movie)
    }

    override suspend fun deleteFavoriteMovie(id: Long) {
        movies.removeIf { movieModel -> movieModel.category == IRepository.Type.FAVORITE && movieModel.id == id }
    }

    override fun observeFavoriteMovies(): DataSource.Factory<Int, MovieModel> {
        val result =
            movies.filter { movieModel -> movieModel.category == IRepository.Type.FAVORITE }
        return FakeDataSource(result)
    }

    override fun observeFavoriteMovie(id: Long): LiveData<MovieModel> {
        val result = MutableLiveData<MovieModel>()
        result.value =
            movies.filter { movieModel -> movieModel.category == IRepository.Type.FAVORITE && movieModel.id == id }[0]
        return result
    }

    override suspend fun getLastPageIndex(): Long =
        if (movies.lastIndex > -1)
            movies[movies.lastIndex].pageIndex else 0

}
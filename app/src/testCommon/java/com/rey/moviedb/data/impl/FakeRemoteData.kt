package com.rey.moviedb.data.impl

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.rey.moviedb.data.IData
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.repository.IRepository
import com.rey.moviedb.util.Result

class FakeRemoteData : IData {

    val popularMovies: MutableList<MovieModel> = mutableListOf()
    val topRatedMovies: MutableList<MovieModel> = mutableListOf()
    val nowPlayingMovies: MutableList<MovieModel> = mutableListOf()
    val movieReviews: MutableList<ReviewModel> = mutableListOf()

    private var showError: Boolean = false
    fun setError(error: Boolean) {
        showError = error
    }

    override suspend fun getPopularMovies(page: Long, apiKey: String): Result<List<MovieModel>> =
        if (!showError)
            Result.Success(popularMovies)
        else Result.Error(Exception("hmm"))

    override suspend fun getTopRatedMovies(page: Long, apiKey: String): Result<List<MovieModel>> =
        if (!showError)
            Result.Success(topRatedMovies)
        else Result.Error(Exception("hmm"))

    override suspend fun getNowPlayingMovies(page: Long, apiKey: String): Result<List<MovieModel>> =
        if (!showError)
            Result.Success(nowPlayingMovies)
        else Result.Error(Exception("hmm"))

    override suspend fun getMovieDetail(id: Long, apiKey: String): Result<MovieModel> =
        if (!showError)
            Result.Success(popularMovies[0].apply {
                posterPath = "fakeUrl"
            })
        else Result.Error(Exception("hmm"))

    override suspend fun getMovieReviews(
        id: Long,
        page: Long,
        apiKey: String
    ): Result<List<ReviewModel>> =
        if (!showError)
            Result.Success(movieReviews)
        else Result.Error(Exception("hmm"))

    override suspend fun insertMoviesCache(list: List<MovieModel>): List<Long> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun insertMovieReviews(list: List<ReviewModel>): List<Long> {
        TODO("Not yet implemented")
    }

    override suspend fun deleteAllMoviesCache(type: IRepository.Type) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun observeMoviesCache(type: IRepository.Type): DataSource.Factory<Int, MovieModel> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun observeMovieDetail(
        type: IRepository.Type,
        id: Long
    ): LiveData<MovieModel> {
        TODO("Not yet implemented")
    }

    override fun observeMovieReviews(id: Long): DataSource.Factory<Int, ReviewModel> {
        TODO("Not yet implemented")
    }

    override suspend fun insertFavoriteMovie(movie: MovieModel) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun deleteFavoriteMovie(
        id: Long
    ) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun observeFavoriteMovies(): DataSource.Factory<Int, MovieModel> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun observeFavoriteMovie(id: Long): LiveData<MovieModel> {
        TODO("Not yet implemented")
    }

    override suspend fun getLastPageIndex(): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
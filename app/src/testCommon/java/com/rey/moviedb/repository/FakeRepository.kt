package com.rey.moviedb.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.rey.moviedb.model.MovieModel
import com.rey.moviedb.model.ReviewModel
import com.rey.moviedb.util.FakeDataSource
import javax.inject.Inject

class FakeRepository @Inject constructor() : IRepository {

    val moviesLive = MutableLiveData<List<MovieModel>>()
    val favoriteMoviesLive = MutableLiveData<List<MovieModel>>()
    val movieReviewsLive = MutableLiveData<List<ReviewModel>>()

    val favoriteMovies: MutableList<MovieModel> = mutableListOf()
    val popularMovies: MutableList<MovieModel> = mutableListOf()
    val topRatedMovies: MutableList<MovieModel> = mutableListOf()
    val nowPlayingMovies: MutableList<MovieModel> = mutableListOf()

    override fun observeMovies(type: IRepository.Type): LiveData<PagedList<MovieModel>> =
        switchMap(moviesLive) {
            FakeDataSource(it.filter { movieModel -> movieModel.category == type }).toLiveData(
                pageSize = 20
            )
        }

    override fun observeFavoriteMovies(): LiveData<PagedList<MovieModel>> =
        switchMap(favoriteMoviesLive) {
            FakeDataSource(it.filter { movieModel -> movieModel.category == IRepository.Type.FAVORITE }).toLiveData(
                pageSize = 20
            )
        }


    override fun observeMovie(type: IRepository.Type, id: Long): LiveData<MovieModel> =
        map(moviesLive) {
            it.filter { movieModel -> movieModel.category == type && movieModel.id == id }[0]
        }

    override suspend fun refreshMovies(type: IRepository.Type) {
        when (type) {
            IRepository.Type.POPULAR -> moviesLive.postValue(popularMovies)
            IRepository.Type.TOP_RATED -> moviesLive.postValue(topRatedMovies)
            IRepository.Type.NOW_PLAYING -> moviesLive.postValue(nowPlayingMovies)
            else -> throw IllegalArgumentException("invalid type")
        }
    }

    override suspend fun insertFavoriteMovie(movie: MovieModel) {
        val favorite = movie.copy()
        favorite.category = IRepository.Type.FAVORITE
        favoriteMovies.add(favorite)

        favoriteMoviesLive.postValue(favoriteMovies)
    }

    override suspend fun deleteFavoriteMovie(id: Long) {
        favoriteMovies.removeIf { movieModel -> movieModel.id == id && movieModel.category == IRepository.Type.FAVORITE }

        favoriteMoviesLive.postValue(favoriteMovies)
    }

    override fun observeFavoriteMovie(id: Long): LiveData<MovieModel> =
        map(favoriteMoviesLive) {
            val result =
                it.filter { movieModel -> movieModel.category == IRepository.Type.FAVORITE && movieModel.id == id }
            if (result.isNotEmpty()) result[0] else null
        }

    override fun observeMovieReviews(id: Long): LiveData<PagedList<ReviewModel>> =
        switchMap(movieReviewsLive) {
            val result = it.filter { rev -> rev.movieId == id }
            FakeDataSource(result).toLiveData(pageSize = 20)
        }

}
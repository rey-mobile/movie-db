package com.rey.moviedb.util

import android.content.Context
import androidx.paging.DataSource
import androidx.paging.PositionalDataSource
import com.google.gson.Gson
import com.rey.moviedb.R
import com.rey.moviedb.model.*
import com.rey.moviedb.repository.IRepository

fun Context.popularItems(): List<MovieModel> = this.popularResponse()!!.results
fun Context.topRatedItems(): List<MovieModel> = this.topRatedResponse()!!.results
fun Context.nowPlayingItems(): List<MovieModel> = this.nowPlayingResponse()!!.results
fun Context.favoriteItems(): List<MovieModel> = this.nowPlayingResponse()!!.results.map {
    it.category = IRepository.Type.FAVORITE
    it
}

fun Context.reviewItems(): List<ReviewModel> {
    val reviews: MutableList<ReviewModel> = mutableListOf()
    reviews.addAll(this.reviewResponse(popularItems()[3].id)!!.results)
    reviews.addAll(this.reviewResponse(popularItems()[1].id)!!.results)

    return reviews.toList()
}

fun Context.reviewItems(id: Long): List<ReviewModel> = this.reviewResponse(id)!!.results

fun Context.popularResponse(): PopularModel? {
    val responseModel = Gson().fromJson(
        this.resources.openRawResource(R.raw.popular_200).reader(),
        PopularModel::class.java
    )

    responseModel.let { response ->
        response.results.map {
            it.category = IRepository.Type.POPULAR
            it.posterPath = BASE_POSTER_URL + it.posterPath
            it.pageIndex = response.page
            it
        }
    }

    return responseModel
}

fun Context.topRatedResponse(): TopRatedModel? {
    val responseModel = Gson().fromJson(
        this.resources.openRawResource(R.raw.top_rated_200).reader(),
        TopRatedModel::class.java
    )

    responseModel.let { response ->
        response.results.map {
            it.category = IRepository.Type.TOP_RATED
            it.posterPath = BASE_POSTER_URL + it.posterPath
            it.pageIndex = response.page
            it
        }
    }

    return responseModel
}

fun Context.nowPlayingResponse(): NowPlayingModel? {
    val responseModel = Gson().fromJson(
        this.resources.openRawResource(R.raw.now_playing_200).reader(),
        NowPlayingModel::class.java
    )

    responseModel.let { response ->
        response.results.map {
            it.category = IRepository.Type.NOW_PLAYING
            it.posterPath = BASE_POSTER_URL + it.posterPath
            it.pageIndex = response.page
            it
        }
    }

    return responseModel
}

fun Context.reviewResponse(id: Long): ReviewsModel? {
    val responseModel = when (id) {
        popularItems()[1].id -> Gson().fromJson(
            resources.openRawResource(R.raw.reviews_512200_200).reader(),
            ReviewsModel::class.java
        )
        popularItems()[3].id -> Gson().fromJson(
            resources.openRawResource(R.raw.reviews_419704_200).reader(),
            ReviewsModel::class.java
        )
        else -> throw IllegalArgumentException("invalid id")
    }

    responseModel.let { response ->
        response.results.map {
            it.movieId = response.id
            it.pageIndex = response.page
        }
        return responseModel
    }
}

class FakeDataSource<T>(val data: List<T>) : DataSource.Factory<Int, T>() {
    override fun create(): DataSource<Int, T> {
        return object : PositionalDataSource<T>() {
            override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<T>) {
            }

            override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<T>) {
                callback.onResult(data, 0, data.size)
            }
        }
    }
}

const val API_KEY = "thisIsNotAWeirdKey"
const val WEIRD_API_KEY = "thisIsAWeirdKey"
const val BASE_POSTER_URL = "https://image.tmdb.org/t/p/w92"